#include <iostream>
#include <vector>

using namespace std;

// Definition for a binary tree node.

struct TreeNode {
     int val;
     TreeNode *left;
     TreeNode *right;
     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
     TreeNode(int x, int y, int z) : val(x), left(nullptr), right(nullptr){
         this->left = new TreeNode(y);
         this->right = new TreeNode(z);
     }
 };


class Solution {
public:
//    vector<vector<int>> pathSum(TreeNode* root, int sum) {
//
//    }

    void printTree(TreeNode* root){

        if (root == nullptr) {
            return ;
        }


        cout << root->val << " ";

        printTree(root->left);
        printTree(root->right);

    }


};

int main(){

    // new tree
    TreeNode *head = new TreeNode(5, 4, 8);
    TreeNode *temp;

    temp = head->left;
    temp->left = new TreeNode(11);
    temp = temp->left;
    temp = new TreeNode(11, 7, 2);

    temp = head->right;
    temp = new TreeNode(8, 13, 4);
    temp = temp->right;
    temp = new TreeNode(4, 5, 1);

    // test

    Solution a;

    a.printTree(head);

    return 0;

}